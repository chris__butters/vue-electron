# Vue + Webpack + Electron = Awesomeness

Basic implementation of Vue with Webpack to compile JS and Electron to wrap into a native app.

Utilizes [electron-builder](http://electron.rocks/building-and-deploying-application/)

### Todo
- [ ] Auto-updater integration
- [ ] Release to server following [this](http://electron.rocks/building-and-deploying-application/)
- [ ] Test on Windows & Linux (should be fine just need to make sure)