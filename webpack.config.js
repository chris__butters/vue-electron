var path = require('path')
var webpack = require('webpack')
var LiveReloadPlugin = require('webpack-livereload-plugin')

module.exports = {
  entry: './src/main.js',
  target: 'electron',
  output: {
    path: path.resolve(__dirname, './app'),
    filename: 'index.js'
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue'
      },
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/
      }
    ]
  },
  babel: {
    "presets": ["es2015"],
    "plugins": ["transform-runtime"]
  },
  plugins: [
    new webpack.ExternalsPlugin('commonjs', [
      'electron'
    ]),
    new webpack.DefinePlugin({ 'process.env.NODE_ENV': '"development"' }),
    new LiveReloadPlugin()
  ]
}
