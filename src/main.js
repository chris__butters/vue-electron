const Vue = require('vue');
const App = require('./App.vue');

new Vue({
  el: '#app',
  components: { App }
})